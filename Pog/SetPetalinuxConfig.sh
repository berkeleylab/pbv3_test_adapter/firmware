#!/bin/bash

# Parse arguments
if [ ${#} != 2 ] && [ ${#} != 3 ]; then
    echo "usage: ${0} config key [value]"
    exit 1
fi
config=${1}
key=${2}
value=${3}

# Check if config file exists
if [ ! -e ${config} ]; then
    echo "Error: Config file not found!"
    exit 1
fi

# Exit if any of these commands fails
set -e

# get current value
current=$(grep ${key}[^_a-zA-Z] ${config} || true)
if [ "x${current}x" == "xx" ]; then # new value
    echo ${key}=${value} >> ${config}
elif [ "x${value}x" == "xx" ]; then # unset value
    sed -i -e "s|${current}|# ${key} is not set|" ${config}
else # change value
    sed -i -e "s|${current}|${key}=${value}|" ${config}
fi

