#!/bin/bash

# Parse arguments
if [ ${#} != 2 ]; then
    echo "usage: ${0} petawork newconfig"
    exit 1
fi
petawork=${1}
newconfig=${2}

# Exit if any of these commands fails
set -e

POGDIR=$(realpath $(dirname "${BASH_SOURCE[0]}"))

# Extract name of target config file
configname=$(basename ${newconfig})
oldconfig=${petawork}/project-spec/configs/${configname}
if [ ! -e ${oldconfig} ]; then
    echo "Error: Trying to patch an unknown config file."
    echo "\t${oldconfig}"
    exit 1
fi

# Loop over values
IFS=$'\n'; for row in $(cat ${newconfig}); do
    # Filter random rows
    if [ "x${row}x" == "xx" ]; then
	continue
    fi

    if [[ ${row} == \#* ]]; then
	continue
    fi

    # Update configuration file
    if [[ ${row} == *=* ]];
    then # set
	key=${row%%=*}
	value=${row#*=}

	${POGDIR}/SetPetalinuxConfig.sh ${oldconfig} ${key} "${value}"
    else # unset
	key=${row}

	${POGDIR}/SetPetalinuxConfig.sh ${oldconfig} ${key}
    fi
done
