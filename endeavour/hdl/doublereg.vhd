library ieee;
use ieee.std_logic_1164.all;

----
-- Entity: doublereg
-- Author: Karol Krizka <kkrizka@gmail.com>
--
-- Double register an input signal.
----
entity doublereg is
  port (
    clock       : in  std_logic;
    reset       : in  std_logic;

    input       : in  std_logic;
    output      : out std_logic
    );
end entity doublereg;

architecture behavioural of doublereg is
  signal reg1   : std_logic;
  signal reg2   : std_logic;
begin
  process(clock)
  begin
    if rising_edge(clock) then
      if reset='1' then
        reg1    <= '0';
        reg2    <= '0';
      else
        reg1    <= input;
        reg2    <= reg1;
      end if;
    end if;
  end process;

  output        <= reg2;
end behavioural;
