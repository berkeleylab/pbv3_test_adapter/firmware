library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.Vcomponents.all;

entity endeavour_top is
  generic (
    -- Users to add parameters here
    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH	: integer	:= 32;
    C_S00_AXI_ADDR_WIDTH	: integer	:= 6
    );
  port (
    -- Users to add ports here
    busy        : out std_logic;
    datavalid   : out std_logic;
    error       : out std_logic;
    CMD_IN_P    : out std_logic;
    CMD_IN_N    : out std_logic;

    CMD_OUT_P   : in std_logic;
    CMD_OUT_N   : in std_logic;

    cmd_in      : out std_logic;
    cmd_out     : out std_logic;
    -- User ports ends
    -- Do not modify the ports beyond this line


    -- Ports of Axi Slave Bus Interface S00_AXI
    s00_axi_aclk	: in std_logic;
    s00_axi_aresetn	: in std_logic;
    s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot	: in std_logic_vector(2 downto 0);
    s00_axi_awvalid	: in std_logic;
    s00_axi_awready	: out std_logic;
    s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid	: in std_logic;
    s00_axi_wready	: out std_logic;
    s00_axi_bresp	: out std_logic_vector(1 downto 0);
    s00_axi_bvalid	: out std_logic;
    s00_axi_bready	: in std_logic;
    s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot	: in std_logic_vector(2 downto 0);
    s00_axi_arvalid	: in std_logic;
    s00_axi_arready	: out std_logic;
    s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp	: out std_logic_vector(1 downto 0);
    s00_axi_rvalid	: out std_logic;
    s00_axi_rready	: in std_logic
    );
end endeavour_top;

architecture arch_imp of endeavour_top is

  -- component declaration
  component endeavour_axi is
    generic (
      C_S_AXI_DATA_WIDTH	: integer	:= 32;
      C_S_AXI_ADDR_WIDTH	: integer	:= 6
      );
    port (
      axi_control       : out std_logic_vector(31 downto 0);
      axi_status        : in  std_logic_vector(31 downto 0);
      axi_nbitsin       : out std_logic_vector(31 downto 0);
      axi_datain        : out std_logic_vector(63 downto 0);
      axi_nbitsout      : in  std_logic_vector(31 downto 0);
      axi_dataout       : in  std_logic_vector(63 downto 0);
      axi_config        : out std_logic_vector(31 downto 0);

      TICKS_DIT_MIN     : out integer range 0 to 4095;
      TICKS_DIT_MID     : out integer range 0 to 4095;
      TICKS_DIT_MAX     : out integer range 0 to 4095;

      TICKS_DAH_MIN     : out integer range 0 to 4095;
      TICKS_DAH_MID     : out integer range 0 to 4095;
      TICKS_DAH_MAX     : out integer range 0 to 4095;

      TICKS_BITGAP_MIN  : out integer range 0 to 4095;
      TICKS_BITGAP_MID  : out integer range 0 to 4095;
      TICKS_BITGAP_MAX  : out integer range 0 to 4095;

      S_AXI_ACLK	: in std_logic;
      S_AXI_ARESETN	: in std_logic;
      S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
      S_AXI_AWVALID	: in std_logic;
      S_AXI_AWREADY	: out std_logic;
      S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
      S_AXI_WVALID	: in std_logic;
      S_AXI_WREADY	: out std_logic;
      S_AXI_BRESP	: out std_logic_vector(1 downto 0);
      S_AXI_BVALID	: out std_logic;
      S_AXI_BREADY	: in std_logic;
      S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
      S_AXI_ARVALID	: in std_logic;
      S_AXI_ARREADY	: out std_logic;
      S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_RRESP	: out std_logic_vector(1 downto 0);
      S_AXI_RVALID	: out std_logic;
      S_AXI_RREADY	: in std_logic
      );
  end component endeavour_axi;
    		
  component endeavour_manager is
    generic (
      TICKS_QUIESCENT : integer := 75
      );
    port (
      clock             : in  std_logic;
      reset             : in  std_logic;
      TICKS_DIT_MIN     : in  integer range 0 to 4095 :=   6;
      TICKS_DIT_MID     : in  integer range 0 to 4095 :=  14;
      TICKS_DIT_MAX     : in  integer range 0 to 4095 :=  22;
      TICKS_DAH_MIN     : in  integer range 0 to 4095 :=  29;
      TICKS_DAH_MID     : in  integer range 0 to 4095 :=  76;
      TICKS_DAH_MAX     : in  integer range 0 to 4095 := 124;
      TICKS_BITGAP_MIN  : in  integer range 0 to 4095 :=  11;
      TICKS_BITGAP_MID  : in  integer range 0 to 4095 :=  43;
      TICKS_BITGAP_MAX  : in  integer range 0 to 4095 :=  75;
      nbitsin           : in  integer range 0 to 63;    
      datain            : in  std_logic_vector(63 downto 0);
      send              : in  std_logic;
      busy              : out std_logic;
      nbitsout          : out integer range 0 to 63;
      dataout           : out std_logic_vector(63 downto 0);
      datavalid         : out std_logic;
      error             : out std_logic;    
      serialin          : in  std_logic;
      serialout         : out std_logic
      );
  end component endeavour_manager;

  component smooth is
    port (
      clock             : in  std_logic;
      reset             : in  std_logic;
      input             : in  std_logic;
      output            : out std_logic
      );
  end component smooth;

  component doublereg is
    port (
      clock             : in  std_logic;
      reset             : in  std_logic;
      input             : in  std_logic;
      output            : out std_logic
      );
  end component doublereg;  

  component TopLevel_clk_wiz_0_0_clk_wiz is
    port (
      clk_out1 : out std_logic;
      clk_in1 : in std_logic
      );
  end component TopLevel_clk_wiz_0_0_clk_wiz;

  --    
  -- signal declarations
  signal clock100MHz    : std_logic;
  signal clock80MHz     : std_logic;
  signal reset          : std_logic;
  signal invert_cmdin   : std_logic;
  signal invert_cmdout  : std_logic;
  signal seriali        : std_logic;
  signal seriali_buf    : std_logic;
  signal seriali_reg    : std_logic;
  signal seriali_noise  : std_logic;
  signal serialo        : std_logic;  
  signal serialo_buf    : std_logic;
  signal axi_control    : std_logic_vector(31 downto 0);
  signal axi_status     : std_logic_vector(31 downto 0);
  signal axi_nbitsin    : std_logic_vector(31 downto 0);
  signal axi_datain     : std_logic_vector(63 downto 0);
  signal axi_nbitsout   : std_logic_vector(31 downto 0);
  signal axi_dataout    : std_logic_vector(63 downto 0);
  signal axi_config     : std_logic_vector(31 downto 0);
  
  signal axi_nbitsout_integer : integer range 0 to 63;
  
  -- configuration signals
  signal TICKS_DIT_MIN     : integer range 0 to 4095;
  signal TICKS_DIT_MID     : integer range 0 to 4095;
  signal TICKS_DIT_MAX     : integer range 0 to 4095;
  signal TICKS_DAH_MIN     : integer range 0 to 4095;
  signal TICKS_DAH_MID     : integer range 0 to 4095;
  signal TICKS_DAH_MAX     : integer range 0 to 4095;
  signal TICKS_BITGAP_MIN  : integer range 0 to 4095;
  signal TICKS_BITGAP_MID  : integer range 0 to 4095;
  signal TICKS_BITGAP_MAX  : integer range 0 to 4095;
begin

-- Instantiation of Axi Bus Interface S00_AXI
  endeavour_axi_inst : endeavour_axi
    generic map (
      C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
      )
    port map (
      axi_control       => axi_control,
      axi_status        => axi_status,
      axi_nbitsin       => axi_nbitsin,
      axi_datain        => axi_datain,
      axi_nbitsout      => axi_nbitsout,
      axi_dataout       => axi_dataout,
      axi_config        => axi_config,
      TICKS_DIT_MIN     => TICKS_DIT_MIN   ,
      TICKS_DIT_MID     => TICKS_DIT_MID   ,
      TICKS_DIT_MAX     => TICKS_DIT_MAX   ,
      TICKS_DAH_MIN     => TICKS_DAH_MIN   ,
      TICKS_DAH_MID     => TICKS_DAH_MID   ,
      TICKS_DAH_MAX     => TICKS_DAH_MAX   ,
      TICKS_BITGAP_MIN  => TICKS_BITGAP_MIN,
      TICKS_BITGAP_MID  => TICKS_BITGAP_MID,
      TICKS_BITGAP_MAX  => TICKS_BITGAP_MAX,
      S_AXI_ACLK	=> s00_axi_aclk,
      S_AXI_ARESETN	=> s00_axi_aresetn,
      S_AXI_AWADDR	=> s00_axi_awaddr,
      S_AXI_AWPROT	=> s00_axi_awprot,
      S_AXI_AWVALID	=> s00_axi_awvalid,
      S_AXI_AWREADY	=> s00_axi_awready,
      S_AXI_WDATA	=> s00_axi_wdata,
      S_AXI_WSTRB	=> s00_axi_wstrb,
      S_AXI_WVALID	=> s00_axi_wvalid,
      S_AXI_WREADY	=> s00_axi_wready,
      S_AXI_BRESP	=> s00_axi_bresp,
      S_AXI_BVALID	=> s00_axi_bvalid,
      S_AXI_BREADY	=> s00_axi_bready,
      S_AXI_ARADDR	=> s00_axi_araddr,
      S_AXI_ARPROT	=> s00_axi_arprot,
      S_AXI_ARVALID	=> s00_axi_arvalid,
      S_AXI_ARREADY	=> s00_axi_arready,
      S_AXI_RDATA	=> s00_axi_rdata,
      S_AXI_RRESP	=> s00_axi_rresp,
      S_AXI_RVALID	=> s00_axi_rvalid,
      S_AXI_RREADY	=> s00_axi_rready
      );

-- Add user logic here
  reset         <= axi_control(0);

  invert_cmdin  <= axi_config(0);
  invert_cmdout <= axi_config(1);
  -- Differential buffers for AMAC communication
  CMD_IN_buf_inst : OBUFDS
    generic map(
      IOSTANDARD => "LVDS_25"
      )
    port map(
      I   => serialo_buf,
      O   => CMD_IN_P,
      OB  => CMD_IN_N
      );
    
  CMD_OUT_buf_inst : IBUFDS
    generic map(
      IOSTANDARD => "LVDS_25"
      )
    port map(
      I   => CMD_OUT_P,
      IB  => CMD_OUT_N,
      O   => seriali_buf
      );

  cmd_in        <= serialo;
  cmd_out       <= seriali;

  serialo_buf   <= serialo     when invert_cmdin ='0' else not serialo;
  seriali       <= seriali_buf when invert_cmdout='0' else not seriali_buf;

  inst_reginput         : doublereg
    port map (
      clock     => clock100MHz,
      reset     => reset,
      input     => seriali,
      output    => seriali_reg
      );

  inst_smoothinput      : smooth
    port map (
      clock     => clock100MHz,
      reset     => reset,
      input     => seriali_reg,
      output    => seriali_noise
      );  
  
  busy        <= axi_status(0);
  datavalid   <= axi_status(1);
  error       <= axi_status(2);
  --
  clock100MHz       <= s00_axi_aclk;
  -- inst_TopLevel_clk_wiz_0_0_clk_wiz : TopLevel_clk_wiz_0_0_clk_wiz
  --   port map(
  --     clk_out1 => clock80MHz,
  --     clk_in1 => clock100MHz
  --     );
    
  inst_endeavour_manager : endeavour_manager
    generic map (
      TICKS_QUIESCENT   => 200
      )
    port map (
      clock             => clock100MHz,
      reset             => reset,
      TICKS_DIT_MIN     => TICKS_DIT_MIN   ,
      TICKS_DIT_MID     => TICKS_DIT_MID   ,
      TICKS_DIT_MAX     => TICKS_DIT_MAX   ,
      TICKS_DAH_MIN     => TICKS_DAH_MIN   ,
      TICKS_DAH_MID     => TICKS_DAH_MID   ,
      TICKS_DAH_MAX     => TICKS_DAH_MAX   ,
      TICKS_BITGAP_MIN  => TICKS_BITGAP_MIN,
      TICKS_BITGAP_MID  => TICKS_BITGAP_MID,
      TICKS_BITGAP_MAX  => TICKS_BITGAP_MAX,
      nbitsin           => to_integer(unsigned(axi_nbitsin)),
      datain            => axi_datain,
      send              => axi_control(1),
      busy              => axi_status(0),
      nbitsout          => axi_nbitsout_integer,
      dataout           => axi_dataout,
      datavalid         => axi_status(1),
      error             => axi_status(2),
      serialin          => seriali_noise,
      serialout         => serialo
      );
  axi_nbitsout  <= std_logic_vector(to_unsigned(axi_nbitsout_integer,axi_nbitsout'length));
    
  -- User logic ends

end arch_imp;
