# I2C
set_property PACKAGE_PIN N16 [get_ports iic_rtl0_scl_io];
set_property PACKAGE_PIN N15 [get_ports iic_rtl0_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl0_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl0_sda_io]

# SPI
set_property PACKAGE_PIN L14 [get_ports {spi_rtl0_ss_io[0]}];
set_property PACKAGE_PIN L15 [get_ports {spi_rtl0_sck_io}];
set_property PACKAGE_PIN K16 [get_ports spi_rtl0_io1_io];
set_property PACKAGE_PIN J16 [get_ports spi_rtl0_io0_io];

set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl0_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl0_ss_io[0]}]

# AMAC
set_property PACKAGE_PIN K14 [get_ports {CMD_OUT_P_0}];
set_property PACKAGE_PIN J14 [get_ports {CMD_OUT_N_0}];
set_property PACKAGE_PIN M14 [get_ports {CMD_IN_P_0}];
set_property PACKAGE_PIN M15 [get_ports {CMD_IN_N_0}];

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_0]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_0]
set_property DIFF_TERM true [get_ports CMD_OUT_N_0]
