# I2C
set_property PACKAGE_PIN V11 [get_ports iic_rtl4_scl_io];
set_property PACKAGE_PIN V10 [get_ports iic_rtl4_sda_io];

set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl4_scl_io]
set_property IOSTANDARD LVCMOS25 [get_ports iic_rtl4_sda_io]

# SPI
set_property PACKAGE_PIN T12 [get_ports {spi_rtl4_ss_io[0]}];
set_property PACKAGE_PIN U12 [get_ports {spi_rtl4_sck_io}];
set_property PACKAGE_PIN V8 [get_ports spi_rtl4_io0_io];
set_property PACKAGE_PIN W8 [get_ports spi_rtl4_io1_io];

set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl4_sck_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl4_io0_io]
set_property IOSTANDARD LVCMOS25 [get_ports spi_rtl4_io1_io]
set_property IOSTANDARD LVCMOS25 [get_ports {spi_rtl4_ss_io[0]}]

# AMAC
set_property PACKAGE_PIN V12 [get_ports {CMD_OUT_P_4}];
set_property PACKAGE_PIN W13 [get_ports {CMD_OUT_N_4}];
set_property PACKAGE_PIN W14 [get_ports {CMD_IN_P_4}];
set_property PACKAGE_PIN Y14 [get_ports {CMD_IN_N_4}];

set_property IOSTANDARD LVDS_25 [get_ports CMD_IN_P_4]
set_property IOSTANDARD LVDS_25 [get_ports CMD_OUT_P_4]
set_property DIFF_TERM true [get_ports CMD_OUT_N_4]
