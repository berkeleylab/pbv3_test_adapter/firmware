# Introduction
Firmware for the PBv3 mass tester active board.

# Requirements
- Vivado 2021.1
- Petalinux 2020.3
- MicroZed or ZTurn 7Z020

# Compiling Firmware
There are several projects available to support the different SoC boards supported by the PBv3.1 test system.
- `zturn_7020`: For use with ZTurn 7z020
- `microzed_7020.1`: For use with MicroZed 7z020 and direct communication (Active Board 20190718)
- `microzed_7020.2`: For use with MicroZed 7z020 and multiplexed communication (Active Board 20210504)

The following will build `project` (one of the above). The `endeavour_ip` package has to be only created once for all projects. Or updated after any change to the endavour IP.
```sh
./Hog/CreateProject.sh endeavour_ip
./Hog/CreateProject.sh project
./Hog/LaunchWorkflow.sh project
```

# Compiling PetaLinux
There are two variations of Petalinux configurations available:
- `sdcard`: Meant for reception testing. Barebones file system stored in the image and loaded as RO.
- `nfs`: Meant for developement and crate work. File system is hosted via NFS and has multiple developement libraries available.

The following will compile the `sdcard` variations for all available fimrmware inside `bin/`. Replace the occurances of the word correspondingly to compile the `nfs` variation.

```sh
./Pog/CreatePetalinux.sh petawork petalinux petalinux_sdcard
./Pog/BuildPetalinux.sh petawork sdcard
```


# Contributors
Karol Krizka <kkrizka@cern.ch>
