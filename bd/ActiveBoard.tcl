################################################################
# CONFIGURE
################################################################

variable script_folder
set script_folder ${repo_path}/bd

################################################################
# START
################################################################

# Hierarchical cell: ActiveBoard
#
# Consists of the following IP's:
#  - I2C
#  - SPI
#  - n x endeavour
#
# If more than one endeavour block is requested, then the CMD output
# ports are suffixed with `_x`, where x goes from 0 to n-1.
#
# @param numEndeavour Number of endeavour blocks to add
proc create_hier_cell_activeboard { parentCell nameHier {numEndeavour 1} } {
    if { $parentCell eq "" || $nameHier eq "" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_activeboard() - Empty argument(s)!"}
	return
    }

    # Get object for parentCell
    set parentObj [get_bd_cells $parentCell]
    if { $parentObj == "" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
	return
    }

    # Make sure parentObj is hier blk
    set parentType [get_property TYPE $parentObj]
    if { $parentType ne "hier" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
	return
    }

    # Save current instance; Restore later
    set oldCurInst [current_bd_instance .]

    # Set parent object as current
    current_bd_instance $parentObj

    # Create cell and set as current instance
    set hier_obj [create_bd_cell -type hier $nameHier]
    current_bd_instance $hier_obj

    # Create interface pins
    create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_i2c_AXI
    create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_spi_AXI
    create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 iic_rtl
    create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 spi_rtl

    # Create pins
    create_bd_pin -dir O -from 1 -to 0 -type intr irpt
    create_bd_pin -dir I -type clk s_axi_aclk
    create_bd_pin -dir I -type rst s_axi_aresetn

    #
    # Block diagrams
    #

    #
    # Interrupt signal concatenate
    set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
    connect_bd_net [get_bd_pins irpt] [get_bd_pins xlconcat_0/dout]

    #
    # I2C

    # IP
    set axi_iic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_iic:2.1 axi_iic_0 ]

    # AXI
    connect_bd_intf_net [get_bd_intf_pins S_i2c_AXI] [get_bd_intf_pins axi_iic_0/S_AXI]

    connect_bd_net -net s_axi_aclk [get_bd_pins s_axi_aclk] [get_bd_pins axi_iic_0/s_axi_aclk]
    connect_bd_net -net s_axi_aresetn [get_bd_pins s_axi_aresetn] [get_bd_pins axi_iic_0/s_axi_aresetn]

    connect_bd_net [get_bd_pins axi_iic_0/iic2intc_irpt] [get_bd_pins xlconcat_0/In0]

    # Connections
    connect_bd_intf_net [get_bd_intf_pins iic_rtl] [get_bd_intf_pins axi_iic_0/IIC]

    #
    # SPI

    # IP
    set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
    set_property -dict [ list \
			     CONFIG.C_USE_STARTUP {0} \
			    ] $axi_quad_spi_0

    # AXI
    connect_bd_intf_net [get_bd_intf_pins S_spi_AXI] [get_bd_intf_pins axi_quad_spi_0/AXI_LITE]

    connect_bd_net -net s_axi_aclk [get_bd_pins s_axi_aclk] [get_bd_pins axi_quad_spi_0/ext_spi_clk] [get_bd_pins axi_quad_spi_0/s_axi_aclk]
    connect_bd_net -net s_axi_aresetn [get_bd_pins s_axi_aresetn] [get_bd_pins axi_quad_spi_0/s_axi_aresetn]

    connect_bd_net [get_bd_pins axi_quad_spi_0/ip2intc_irpt] [get_bd_pins xlconcat_0/In1]

    # Connections
    connect_bd_intf_net [get_bd_intf_pins spi_rtl] [get_bd_intf_pins axi_quad_spi_0/SPI_0]

    #
    # Endeavour

    for {set i 0} {$i < $numEndeavour} {incr i} {
	if { ${numEndeavour}==1 } {
	    set suffix ""
	} else {
	    set suffix "_PB${i}"
	}

	# Interface
	set S_end_AXI [create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_end_AXI${suffix}]
	
	# Ports
	create_bd_pin -dir O CMD_IN_N${suffix}
	create_bd_pin -dir O CMD_IN_P${suffix}
	create_bd_pin -dir I CMD_OUT_N${suffix}
	create_bd_pin -dir I CMD_OUT_P${suffix}

	# IP
	create_bd_cell -type ip -vlnv lbl.gov:pbv3:endeavour:1.0 axi_endeavour${suffix}

	# AXI
	connect_bd_intf_net ${S_end_AXI} [get_bd_intf_pins axi_endeavour${suffix}/s00_axi]

	# connect_bd_net -net s_axi_aclk [get_bd_pins s_axi_aclk] [get_bd_pins axi_endeavour${suffix}/s00_axi_aclk]
	# connect_bd_net -net s_axi_aresetn [get_bd_pins s_axi_aresetn] [get_bd_pins axi_endeavour${suffix}/s00_axi_aresetn]

	# Connections
	connect_bd_net -net CMD_OUT_N${suffix} [get_bd_pins CMD_OUT_N${suffix}] [get_bd_pins axi_endeavour${suffix}/CMD_OUT_N]
	connect_bd_net -net CMD_OUT_P${suffix} [get_bd_pins CMD_OUT_P${suffix}] [get_bd_pins axi_endeavour${suffix}/CMD_OUT_P]
	connect_bd_net -net CMD_IN_N${suffix}  [get_bd_pins CMD_IN_N${suffix} ] [get_bd_pins axi_endeavour${suffix}/CMD_IN_N ]
	connect_bd_net -net CMD_IN_P${suffix}  [get_bd_pins CMD_IN_P${suffix} ] [get_bd_pins axi_endeavour${suffix}/CMD_IN_P ]
    }

    # Restore current instance
    current_bd_instance $oldCurInst
}

# Connect hierarchical cell: ActiveBoard
#
# @param parentCell Name of top level cell with cells to be connected
# @param pbCell Name of activeboard cell 
# @param AXIport First available AXI port. Also used for interrupt concat.
# @param abNum Active board number.
# @param numEndeavour Number of endeavour blocks to connect
proc connect_hier_cell_activeboard { parentCell pbCell AXIport abNum {numEndeavour 1}} {
    if { $parentCell eq "" } {
	set parentCell [get_bd_cells /]
    }

    # Get object for parentCell
    set parentObj [get_bd_cells $parentCell]
    if { $parentObj == "" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
	return
    }

    # Make sure parentObj is hier blk
    set parentType [get_property TYPE $parentObj]
    if { $parentType ne "hier" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
	return
    }

    # Save current instance; Restore later
    set oldCurInst [current_bd_instance .]

    # Set parent object as current
    current_bd_instance $parentObj

    # Interfaces
    create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 iic_rtl${abNum}
    create_bd_intf_port -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 spi_rtl${abNum}

    connect_bd_intf_net [get_bd_intf_ports iic_rtl${abNum}] [get_bd_intf_pins ${pbCell}/iic_rtl]
    connect_bd_intf_net [get_bd_intf_ports spi_rtl${abNum}] [get_bd_intf_pins ${pbCell}/spi_rtl]

    # Endeavour Ports
    for {set i 0} {$i < $numEndeavour} {incr i} {
	if { ${numEndeavour}==1 } {
	    set suffix ""
	} else {
	    set suffix "_PB${i}"
	}

	create_bd_port -dir O CMD_IN_N_${abNum}${suffix}
	create_bd_port -dir O CMD_IN_P_${abNum}${suffix}
	create_bd_port -dir I CMD_OUT_N_${abNum}${suffix}
	create_bd_port -dir I CMD_OUT_P_${abNum}${suffix}

	connect_bd_net [get_bd_ports CMD_IN_N_${abNum}${suffix} ] [get_bd_pins ${pbCell}/CMD_IN_N${suffix} ]
	connect_bd_net [get_bd_ports CMD_IN_P_${abNum}${suffix} ] [get_bd_pins ${pbCell}/CMD_IN_P${suffix} ]
	connect_bd_net [get_bd_ports CMD_OUT_N_${abNum}${suffix}] [get_bd_pins ${pbCell}/CMD_OUT_N${suffix}]
	connect_bd_net [get_bd_ports CMD_OUT_P_${abNum}${suffix}] [get_bd_pins ${pbCell}/CMD_OUT_P${suffix}]

	# AXI connections
	set M_end [format "M%02d" [expr ${AXIport} + 2 + ${i}]]

	connect_bd_intf_net [get_bd_intf_pins ${pbCell}/S_end_AXI${suffix}] [get_bd_intf_pins axi_interconnect_0/${M_end}_AXI]

	apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	    -config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	    [get_bd_pins axi_interconnect_0/${M_end}_ACLK]

	assign_bd_address -offset 0x43${abNum}${i}0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${pbCell}/axi_endeavour${suffix}/s00_axi/reg0] -force	
    }

    # AXI
    set M_i2c [format "M%02d" [expr ${AXIport} + 0]]
    set M_spi [format "M%02d" [expr ${AXIport} + 1]]
    connect_bd_intf_net [get_bd_intf_pins ${pbCell}/S_i2c_AXI] [get_bd_intf_pins axi_interconnect_0/${M_i2c}_AXI]
    connect_bd_intf_net [get_bd_intf_pins ${pbCell}/S_spi_AXI] [get_bd_intf_pins axi_interconnect_0/${M_spi}_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	-config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	[get_bd_pins axi_interconnect_0/${M_i2c}_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	-config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	[get_bd_pins axi_interconnect_0/${M_spi}_ACLK]

    assign_bd_address -offset 0x416${abNum}0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${pbCell}/axi_iic_0/S_AXI/Reg] -force
    assign_bd_address -offset 0x41E${abNum}0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${pbCell}/axi_quad_spi_0/AXI_LITE/Reg] -force
    
    connect_bd_net [get_bd_pins ${pbCell}/irpt] [get_bd_pins xlconcat_0/In${abNum}]

    # Restore current instance
    current_bd_instance $oldCurInst
}


# Procedure to create the entire design
#
# Creates:
#  - processing system IP
#  - AXI control and related reset procedures
#  - n x active board block configured for direct communication
#  - n x active board block configured for muxed communicaiton
#
# @param parentCell If "", will use root.
# @param numDirect Number of direct comm active boards
# @param numDaisyChain Number of multiplexed comm active boards
#
proc create_root_design { parentCell numDirect numDaisyChain } {

    variable script_folder

    if { $parentCell eq "" } {
	set parentCell [get_bd_cells /]
    }

    # Get object for parentCell
    set parentObj [get_bd_cells $parentCell]
    if { $parentObj == "" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
	return
    }

    # Make sure parentObj is hier blk
    set parentType [get_property TYPE $parentObj]
    if { $parentType ne "hier" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
	return
    }

    # Save current instance; Restore later
    set oldCurInst [current_bd_instance .]

    # Set parent object as current
    current_bd_instance $parentObj

    #
    # Create Processing System and corresponding AXI bus

    # Processing System
    set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 \
	-config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" } \
	[get_bd_cells processing_system7_0]
    set_property -dict [list \
			    CONFIG.PCW_USE_M_AXI_GP0 {1} \
			    CONFIG.PCW_USE_S_AXI_HP0 {0} \
			    CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
			    CONFIG.PCW_IRQ_F2P_INTR {1} \
			   ]  ${processing_system7_0}

    # AXI
    set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0 ]
    set_property -dict [ list \
			     CONFIG.NUM_MI [expr ${numDirect}*12 + ${numDaisyChain}*3] \
			    ] $axi_interconnect_0

    connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	-config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	[get_bd_pins axi_interconnect_0/ACLK]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	-config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	[get_bd_pins axi_interconnect_0/S00_ACLK]

    # Interrupt
    set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
    set_property -dict [ list \
			     CONFIG.NUM_PORTS [expr ${numDirect} + ${numDaisyChain}] \
			    ] $xlconcat_0

    connect_bd_net [get_bd_pins xlconcat_0/dout] [get_bd_pins processing_system7_0/IRQ_F2P]

    #
    # Create active boards
    set abNum 0
    set AXIport 0

    # Direct connection
    for {set i 0} {$i < $numDirect} {incr i} {
	catch {common::send_gid_msg -ssname ActiveBoard-0 -severity "INFO" "Creating Active Board ${abNum} (direct connection)"}

	create_hier_cell_activeboard  [current_bd_instance .] activeboard${abNum} 10
	connect_hier_cell_activeboard [current_bd_instance .] activeboard${abNum} ${AXIport} ${abNum} 10

	incr abNum
	set AXIport [expr ${AXIport} + 12]
    }

    # Daisy chain
    for {set i 0} {$i < $numDaisyChain} {incr i} {
	catch {common::send_gid_msg -ssname ActiveBoard-0 -severity "INFO" "Creating Active Board ${abNum} (daisy chain)"}

	create_hier_cell_activeboard  [current_bd_instance .] activeboard${abNum}
	connect_hier_cell_activeboard [current_bd_instance .] activeboard${abNum} ${AXIport} ${abNum}

	incr abNum
	set AXIport [expr ${AXIport} + 3]
    }

    # Restore current instance
    current_bd_instance $oldCurInst

    save_bd_design
}
