v0.2.30
- Deal with powerboards with stickers

v0.2.29
- Add a tab to display test results

v0.2.28 
- Update to powergui_1.9.3
