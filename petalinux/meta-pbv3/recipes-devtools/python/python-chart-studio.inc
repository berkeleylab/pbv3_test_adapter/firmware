HOMEPAGE = "https://plot.ly/python/"
SUMMARY = "Utilities for interfacing with plotly's Chart Studio"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://setup.py;md5=7358d577a2c0459c2d4fc68c6ecdf569"


inherit pypi
RDEPENDS_${PN} += " \
    ${PYTHON_PN}-plotly \
    ${PYTHON_PN}-requests \
    ${PYTHON_PN}-retrying \
    ${PYTHON_PN}-six \
"