HOMEPAGE = "https://github.com/ionrock/cachecontrol"
SUMMARY = "httplib2 caching for requests"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=6dc7e1b428eda03d850209fdbd6c71f1"

SRC_URI[md5sum] = "5890b797f9b48b2b4cd1448cca89e396"
SRC_URI[sha256sum] = "be9aa45477a134aee56c8fac518627e1154df063e85f67d4f83ce0ccc23688e8"
BPN="CacheControl"


inherit pypi
RDEPENDS_${PN} += " \
    ${PYTHON_PN}-requests \
    ${PYTHON_PN}-msgpack \
    ${PYTHON_PN}-lockfile \
"