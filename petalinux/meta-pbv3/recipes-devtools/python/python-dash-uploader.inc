HOMEPAGE = "https://github.com/np-8/dash-uploader"
SUMMARY = "The upload package for Dash applications using large data files."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://setup.py;md5=abaa862da9d913d206d97528dcd42752"

BPN="dash_uploader"
inherit pypi

RDEPENDS_${PN} += " \
    ${PYTHON_PN}-dash \
"
