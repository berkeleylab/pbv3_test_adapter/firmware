HOMEPAGE = "https://gitlab.cern.ch/atlas-itk/sw/db/itkdb"
SUMMARY = "Python wrapper to interface with ITk DB."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=78340a4bda6e59f27d9cb621cd767beb"


BPN="itkdb"
inherit pypi

DEPENDS += " \
        ${PYTHON_PN}-setuptools-scm-git-archive-native \
"

RDEPENDS_${PN} += " \
    ${PYTHON_PN}-requests \
    ${PYTHON_PN}-certifi \
    ${PYTHON_PN}-cachecontrol \
    ${PYTHON_PN}-click \
    ${PYTHON_PN}-python-jose \
    ${PYTHON_PN}-attrs \
    ${PYTHON_PN}-python-dotenv \
    ${PYTHON_PN}-simple-settings \
"

