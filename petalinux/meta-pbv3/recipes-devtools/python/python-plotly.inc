HOMEPAGE = "https://plotly.com/python/"
SUMMARY = "An open-source, interactive data visualization library for Python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=c7b311a6fbf8f1e2f22c16e2ad556f98"

inherit pypi

RDEPENDS_${PN} += " \
    ${PYTHON_PN}-retrying \
    ${PYTHON_PN}-six \
"

FILES_${PN} += " \
  ${datadir}/etc/jupyter/* \
  ${datadir}/jupyter/* \
"