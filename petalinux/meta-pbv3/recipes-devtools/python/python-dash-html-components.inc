HOMEPAGE = "https://github.com/plotly/dash-html-components"
SUMMARY = "Vanilla HTML components for Dash"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.md;md5=de72fe4413e3289097c13433a4c97c8e"

BPN="dash_html_components"

inherit pypi
