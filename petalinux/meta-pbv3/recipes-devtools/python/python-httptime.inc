HOMEPAGE = "https://gitlab.cern.ch/atlas-itk/sw/db/itkdb"
SUMMARY = "Module for timing http queries inside a python program."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"

BPN="httptime"
inherit pypi

DEPENDS += " \
        ${PYTHON_PN}-setuptools-scm-git-archive-native \
"
