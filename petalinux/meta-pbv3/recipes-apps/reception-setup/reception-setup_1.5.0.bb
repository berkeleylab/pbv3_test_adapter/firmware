#
# Setup a reception site testing system
#

SUMMARY = "Reception site tester setup"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://receptionsetup file://configs"
SRCREV = "${AUTOREV}"

RDEPENDS_${PN} = " \
	sudo \
"

INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "receptionsetup"

inherit update-rc.d

do_install_append() {
    # Install init script
    install -d ${D}${INIT_D_DIR}
    install -m 0755 ${WORKDIR}/receptionsetup ${D}${INIT_D_DIR}/receptionsetup

    # sudo config for petalinux user to do stuff
    install -m 0750 -d ${D}${sysconfdir}/sudoers.d
    install -m 0644 ${WORKDIR}/configs ${D}${sysconfdir}/sudoers.d/configs

    # File containing release number
    echo ${PV} > ${D}${sysconfdir}/pwb-release
}

FILES_${PN} += "${INIT_D_DIR}/receptionsetup"
