#
# This file is the libpicoipp recipe.
#

SUMMARY = "The libpicoipp library from PicoTech"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "https://labs.picotech.com/debian/pool/main/libp/${BPN}/${BPN}_${PV}_armhf.deb"
SRC_URI[md5sum] = "7f86e699ba5e303400bcc2cc33bca883"

S = "${WORKDIR}"

#RDEPENDS_${PN} = "bash libusb1"

INSANE_SKIP_${PN} = "ldflags"

do_unpack() {
	    ar x ${DL_DIR}/${PN}_${PV}_armhf.deb
}

do_install() {
	     tar -xvf data.tar.gz --directory ${D}
}

FILES_${PN}="/etc/* /usr/share/* /opt/picoscope/lib/*.so.* /opt/picoscope/share/*"
FILES_${PN}-dev="/opt/picoscope/lib/*.so"
