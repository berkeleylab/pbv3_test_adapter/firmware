#
# This file is the PowerGUI recipe.
#

SUMMARY = "Powerboard testing GUI"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "gitsm://gitlab.cern.ch/berkeleylab/pbv3_test_adapter/reporting.git;protocol=https \
file://pwbgui \
file://pwbgui.json"
SRCREV="v${PV}"

S = "${WORKDIR}/git"

INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "pwbgui"

inherit setuptools3 update-rc.d

RDEPENDS_${PN} = " \
	      ${PYTHON_PN}-pandas \
	      ${PYTHON_PN}-matplotlib \
	      ${PYTHON_PN}-dash \
	      ${PYTHON_PN}-dash-auth \
	      ${PYTHON_PN}-dash-uploader \
	      ${PYTHON_PN}-dash-table \
	      ${PYTHON_PN}-itkdb (>=0.3.14) \
	      ${PYTHON_PN}-httptime \
"

do_install_append() {
    # Install init script
    install -d ${D}${INIT_D_DIR}
    install -m 0755 ${WORKDIR}/pwbgui ${D}${INIT_D_DIR}/pwbgui

    # Install configurations
    install -d ${D}${sysconfdir}/powertools
    install -m 0644 ${WORKDIR}/pwbgui.json ${D}${sysconfdir}/powertools/pwbgui.json
}

FILES_${PN} += "${INIT_D_DIR}/pwbgui ${sysconfdir}/powertools/pwbgui.json"
