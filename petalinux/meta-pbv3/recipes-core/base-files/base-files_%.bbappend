dirs755 += "/data"

do_install_append() {
    echo "/dev/mmcblk0p2 /data auto defaults 0 0" >> ${D}${sysconfdir}/fstab
    echo "overlay /etc overlay lowerdir=/etc,upperdir=/data/upper,workdir=/data/work 0 0" >> ${D}${sysconfdir}/fstab
}
